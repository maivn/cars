package api

import (
	"car/internal/domain"
	"encoding/json"
	"errors"
	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
	"io"
	"net/http"
	"net/url"
	"os"
	"strconv"
)

func (a *Api) CarGet(c *gin.Context) {
	//создаем переменную фильтра
	var filter domain.CarFilter

	//биндим данные в переменную
	err := c.BindQuery(&filter)
	if err != nil {
		logrus.Errorln(err.Error())
		c.JSON(http.StatusInternalServerError, err.Error())
		return
	}

	logrus.Infof("фильтр: %+v", filter)

	//получаем тачки по фильтру
	cars, err := a.service.GetByFilter(filter)
	if err != nil {
		logrus.Errorln(err.Error())
		c.JSON(http.StatusInternalServerError, err.Error())
		return
	}

	logrus.Infof("найдены машины: %+v", cars)

	//возвращаем машины
	c.JSON(http.StatusOK, cars)
}

func (a *Api) CarPost(c *gin.Context) {
	//создаем переменную для параметров
	var data struct {
		RegNums []string `json:"regNums" validate:"required"`
	}

	//биндим данные в переменную
	err := c.Bind(&data)
	if err != nil {
		logrus.Errorln(err.Error())
		c.JSON(http.StatusInternalServerError, err.Error())
		return
	}

	//валидация
	err = a.validate.Struct(&data)
	if err != nil {
		logrus.Errorln(err.Error())
		c.JSON(http.StatusBadRequest, err.Error())
		return
	}

	logrus.Infof("рег. номера для создания авто: %+v", data)

	//создаем массив машин для возврата
	var cars domain.Cars

	//обходим номера
	for _, regNum := range data.RegNums {
		//получаем инфу о тачке по номеру
		car, err := getCarInfo(regNum)
		if err != nil {
			logrus.Errorln(err.Error())
			c.JSON(http.StatusInternalServerError, err.Error())
			return
		}

		//если инфа есть сохраняем в бд и кладем в переменную для возврата
		if car != nil {
			logrus.Infof("по номеру %s получено: %+v", regNum, car)
			err = a.service.Save(car)
			if err != nil {
				logrus.Errorln(err.Error())
				c.JSON(http.StatusInternalServerError, err.Error())
				return
			}
			logrus.Infof("авто сохранено в БД: %+v", car)
			cars = append(cars, *car)
		} else {
			logrus.Infof("по номеру %s авто не найдено, будет проигнорировано", regNum)
		}
	}

	//возвращаем массив добавленных тачек
	c.JSON(http.StatusOK, cars)
}

func (a *Api) CarPatch(c *gin.Context) {
	//создаем переменную для данных
	var data struct {
		Cars []struct {
			Id     int    `json:"id" validate:"required"`
			RegNum string `json:"reg_num" validate:"required"`
			Mark   string `json:"mark" validate:"required"`
			Model  string `json:"model" validate:"required"`
			Year   int    `json:"year"`
		} `validate:"required,dive"`
	}

	//биндим данные в переменную
	err := c.Bind(&data.Cars)
	if err != nil {
		logrus.Errorln(err.Error())
		c.JSON(http.StatusInternalServerError, err.Error())
		return
	}

	logrus.Infof("обновляемые авто: %+v", data.Cars)

	//валидация
	err = a.validate.Struct(&data)
	if err != nil {
		logrus.Errorln(err.Error())
		c.JSON(http.StatusBadRequest, err.Error())
		return
	}

	//собираем id обновляемых машин для получения из бд
	var ids []int
	for _, rawCar := range data.Cars {
		ids = append(ids, rawCar.Id)
	}

	//получаем машины из бд по ids
	cars, err := a.service.CarService.GetByIds(ids)
	if err != nil {
		logrus.Errorln(err.Error())
		c.JSON(http.StatusInternalServerError, err.Error())
		return
	}
	logrus.Infof("найденные в БД авто: %+v", cars)

	//обходим обновляемые машины
	for _, rawCar := range data.Cars {
		// и если они есть бд - обновляем
		car := cars.GetById(rawCar.Id)
		if car != nil {
			car.RegNum = rawCar.RegNum
			car.Mark = rawCar.Mark
			car.Model = rawCar.Model
			car.Year = rawCar.Year

			err = a.service.CarService.Save(car)
			if err != nil {
				logrus.Errorln(err.Error())
				c.JSON(http.StatusInternalServerError, err.Error())
				return
			}
			logrus.Infof("авто обновлено: %+v", car)
		} else {
			logrus.Infof("авто нет в базе, будет проигнорировано %+v", car)
		}
	}

	//вернем обновленные данные
	c.JSON(http.StatusOK, cars)
}

func (a *Api) CarPut(c *gin.Context) {
	//получаем id записи из url
	id, _ := strconv.Atoi(c.Param("id"))

	//валидируем id
	err := a.validate.Var(id, "required")
	if err != nil {
		logrus.Errorln(err.Error())
		c.JSON(http.StatusBadRequest, err.Error())
		return
	}

	//создаем переменную для данных
	var data struct {
		RegNum string `json:"reg_num" validate:"required"`
		Mark   string `json:"mark" validate:"required"`
		Model  string `json:"model" validate:"required"`
		Year   int    `json:"year"`
	}

	//биндим данные в переменную
	err = c.Bind(&data)
	if err != nil {
		logrus.Errorln(err.Error())
		c.JSON(http.StatusInternalServerError, err.Error())
		return
	}
	logrus.Infof("новые данные для авто: %+v", data)

	//валидация
	err = a.validate.Struct(&data)
	if err != nil {
		logrus.Errorln(err.Error())
		c.JSON(http.StatusBadRequest, err.Error())
		return
	}

	//получаем авто из БД
	car, err := a.service.CarService.GetById(id)
	if err != nil {
		logrus.Errorln(err.Error())
		c.JSON(http.StatusBadRequest, err.Error())
		return
	}

	//если авто нет - возвращаем
	if car == nil {
		err = errors.New("Авто не найдено. ")
		logrus.Errorln(err.Error())
		c.JSON(http.StatusNotFound, err.Error())
		return
	}

	//обновляем данные
	car.Id = id
	car.RegNum = data.RegNum
	car.Mark = data.Mark
	car.Model = data.Model
	car.Year = data.Year

	//сохраняем авто
	err = a.service.CarService.Save(car)
	if err != nil {
		logrus.Errorln(err.Error())
		c.JSON(http.StatusInternalServerError, err.Error())
		return
	}

	//вернем обновленные данные
	c.JSON(http.StatusOK, car)
}

func (a *Api) CarDelete(c *gin.Context) {
	//получаем id записи из url
	id, _ := strconv.Atoi(c.Param("id"))

	//валидируем id
	err := a.validate.Var(id, "required")
	if err != nil {
		logrus.Errorln(err.Error())
		c.JSON(http.StatusBadRequest, err.Error())
		return
	}
	logrus.Infof("id для удаления: %d", id)

	//получаем авто из БД
	car, err := a.service.CarService.GetById(id)
	if err != nil {
		logrus.Errorln(err.Error())
		c.JSON(http.StatusBadRequest, err.Error())
		return
	}

	//если авто нет - возвращаем
	if car == nil {
		err = errors.New("Авто не найдено. ")
		logrus.Errorln(err.Error())
		c.JSON(http.StatusNotFound, err.Error())
		return
	}

	//удаляем машину по id
	err = a.service.DeleteById(id)
	if err != nil {
		logrus.Errorln(err.Error())
		c.JSON(http.StatusInternalServerError, err.Error())
		return
	}

	logrus.Infof("авто успешно удалено: %d", id)

	//возвращаем результат
	c.JSON(http.StatusOK, true)
}

func getCarInfo(regNum string) (*domain.Car, error) {
	v := url.Values{"regNum": []string{regNum}}
	u := url.URL{
		Scheme:   "https",
		Host:     os.Getenv("API_HOST"),
		Path:     "/info",
		RawQuery: v.Encode(),
	}

	resp, err := http.Get(u.String())
	if err != nil {
		return nil, err
	}

	body, err := io.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}

	var car domain.Car
	err = json.Unmarshal(body, &car)
	if err != nil {
		return nil, err
	}

	return &car, nil
}

/*
type Mark struct {
	Mark   string
	Models []string
}

type Marks []Mark

func randomCar(regNum string) (*domain.Car, error) {
	models := Marks{}
	models = append(models, Mark{
		Mark:   "Kia",
		Models: []string{"Rio", "Ceed", "Sportage", "Optima"},
	})
	models = append(models, Mark{
		Mark:   "Hyundai",
		Models: []string{"Solaris", "Tucson", "Creta", "SantaFe"},
	})

	m := models[rand.Intn(2)]

	return &domain.Car{
		RegNum: regNum,
		Mark:   m.Mark,
		Model:  m.Models[rand.Intn(4)],
		Year:   2000 + rand.Intn(20),
	}, nil
}
*/
