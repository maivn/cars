package api

import (
	"car/internal/service"
	"github.com/go-playground/validator/v10"
)

type Api struct {
	service  service.Service
	validate *validator.Validate
}

func NewApi(service service.Service, validate *validator.Validate) *Api {
	return &Api{
		service:  service,
		validate: validate,
	}
}
