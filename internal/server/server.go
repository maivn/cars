package server

import (
	"car/internal/api"
	"github.com/gin-gonic/gin"
	"os"
)

func RunServer(api *api.Api) {
	srv := gin.Default()
	carGroup := srv.Group("/car")
	{
		carGroup.GET("", api.CarGet)
		carGroup.POST("", api.CarPost)
		carGroup.PATCH("", api.CarPatch)
		carGroup.PUT("/:id", api.CarPut)
		carGroup.DELETE("/:id", api.CarDelete)
	}

	srv.Run(":" + os.Getenv("APP_PORT"))
}
