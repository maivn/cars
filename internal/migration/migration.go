package migration

import (
	"github.com/go-gormigrate/gormigrate/v2"
	"github.com/sirupsen/logrus"
	"gorm.io/gorm"
)

func RegisterMigration(db *gorm.DB) {
	m := gormigrate.New(db, gormigrate.DefaultOptions, []*gormigrate.Migration{
		{
			ID: "car",
			Migrate: func(db *gorm.DB) error {
				type Car struct {
					Id     int    `gorm:"primaryKey,not null"`
					RegNum string `gorm:"not null"`
					Mark   string `gorm:"not null"`
					Model  string `gorm:"not null"`
					Year   int    `gorm:"not null"`
				}
				return db.AutoMigrate(&Car{})
			},
			Rollback: func(db *gorm.DB) error {
				return db.Migrator().DropTable("car")
			},
		},
	})

	err := m.Migrate()
	if err != nil {
		logrus.Panicln("Error migrate:", err.Error())
	} else {
		logrus.Debugln("Миграция выполнена успешно")
	}
}
