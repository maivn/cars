package service

import (
	"car/internal/domain"
	"go.uber.org/dig"
)

type Service struct {
	dig.In
	domain.CarService
}
