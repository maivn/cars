package service

import (
	"car/internal/domain"
)

type carService struct {
	carRepository domain.CarRepository
}

func NewCarService(carRepository domain.CarRepository) domain.CarService {
	return &carService{carRepository: carRepository}
}

func (r *carService) Save(car *domain.Car) error {
	return r.carRepository.Save(car)
}

func (r *carService) GetById(id int) (*domain.Car, error) {
	return r.carRepository.GetById(id)
}

func (r *carService) GetByIds(ids []int) (*domain.Cars, error) {
	return r.carRepository.GetByIds(ids)
}

func (r *carService) GetByFilter(filter domain.CarFilter) (*domain.Cars, error) {
	return r.carRepository.GetByFilter(filter)
}

func (r *carService) DeleteById(id int) error {
	return r.carRepository.DeleteById(id)
}
