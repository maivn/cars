package domain

type Cars []Car

type Car struct {
	Id     int    `gorm:"primaryKey" json:"id"`
	RegNum string `gorm:"not null" json:"reg_num"`
	Mark   string `gorm:"not null" json:"mark"`
	Model  string `gorm:"not null" json:"model"`
	Year   int    `json:"year"`
}

type CarFilter struct {
	Page   int    `form:"page"`
	Limit  int    `form:"limit"`
	RegNum string `form:"reg_num"`
	Mark   string `form:"mark"`
	Model  string `form:"model"`
	Year   int    `form:"year"`
}

type CarRepository interface {
	Save(car *Car) error
	GetById(ids int) (*Car, error)
	GetByIds(ids []int) (*Cars, error)
	GetByFilter(filter CarFilter) (*Cars, error)
	DeleteById(id int) error
}

type CarService interface {
	Save(car *Car) error
	GetById(ids int) (*Car, error)
	GetByIds(ids []int) (*Cars, error)
	GetByFilter(filter CarFilter) (*Cars, error)
	DeleteById(id int) error
}

func (cars *Cars) GetById(id int) *Car {
	for i, car := range *cars {
		if car.Id == id {
			return &(*cars)[i]
		}
	}
	return nil
}
