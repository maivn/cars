package db

import (
	"car/internal/domain"
	"gorm.io/gorm"
)

const carsLimit = 50

type carRepository struct {
	db *gorm.DB
}

func NewCarRepository(db *gorm.DB) domain.CarRepository {
	return &carRepository{db: db}
}

func (r *carRepository) Save(car *domain.Car) error {
	return r.db.Save(car).Error
}

func (r *carRepository) GetById(id int) (*domain.Car, error) {
	var car domain.Car
	err := r.db.Where("id = ?", id).First(&car).Error
	if err == gorm.ErrRecordNotFound {
		return nil, nil
	}
	return &car, err
}

func (r *carRepository) GetByIds(ids []int) (*domain.Cars, error) {
	var cars domain.Cars
	err := r.db.Where("id IN (?)", ids).Find(&cars).Error
	return &cars, err
}

func (r *carRepository) GetByFilter(filter domain.CarFilter) (*domain.Cars, error) {
	limit := carsLimit
	if filter.Limit != 0 && filter.Limit < limit {
		limit = filter.Limit
	}
	cmd := r.db.Limit(limit).Order("id asc")

	if filter.Page != 0 {
		cmd = cmd.Offset(filter.Page * limit)
	}

	if filter.RegNum != "" {
		cmd = cmd.Where("reg_num = ?", filter.RegNum)
	}

	if filter.Mark != "" {
		cmd = cmd.Where("mark = ?", filter.Mark)
	}

	if filter.Model != "" {
		cmd = cmd.Where("model = ?", filter.Model)
	}

	if filter.Year != 0 {
		cmd = cmd.Where("year = ?", filter.Year)
	}

	var cars domain.Cars
	err := cmd.Find(&cars).Error
	return &cars, err
}

func (r *carRepository) DeleteById(id int) error {
	return r.db.Where("id = ?", id).Delete(&domain.Car{}).Error
}
