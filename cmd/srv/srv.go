package main

import (
	"car/config"
	"car/internal/api"
	"car/internal/db"
	"car/internal/migration"
	"car/internal/server"
	"car/internal/service"
	"car/validate"
	"github.com/sirupsen/logrus"
	"go.uber.org/dig"
)

func main() {
	logrus.SetLevel(logrus.DebugLevel)

	c := dig.New()
	err := c.Invoke(config.NewConfig)
	if err != nil {
		logrus.Panicln(err)
	}

	err = c.Provide(db.NewDB)
	if err != nil {
		logrus.Panicln(err)
	}

	err = c.Invoke(migration.RegisterMigration)
	if err != nil {
		logrus.Panicln(err)
	}

	err = c.Provide(validate.NewValidate)
	if err != nil {
		logrus.Panicln(err)
	}

	err = c.Provide(db.NewCarRepository)
	if err != nil {
		logrus.Panicln(err)
	}

	err = c.Provide(service.NewCarService)
	if err != nil {
		logrus.Panicln(err)
	}

	err = c.Provide(api.NewApi)
	if err != nil {
		logrus.Panicln(err)
	}

	err = c.Invoke(server.RunServer)
	if err != nil {
		logrus.Panicln(err)
	}
}
