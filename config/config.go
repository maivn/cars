package config

import (
	"github.com/sirupsen/logrus"
	"github.com/subosito/gotenv"
)

func NewConfig() {
	err := gotenv.Load()
	if err != nil {
		logrus.Panicln(err)
	} else {
		logrus.Debugln("Переменные окружения загружены")
	}
}
